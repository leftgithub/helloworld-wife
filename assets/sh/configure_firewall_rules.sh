#!/bin/bash

# Function to check if a rule exists
rule_exists() {
    local rule="$1"
    sudo ufw status | grep -q "$rule"
}

# Check if at least one service parameter is provided
if [ $# -eq 0 ]; then
  echo "Usage: $0 {RDP|SSH} [RDP SSH]"
  exit 1
fi

# Install ufw if it's not already installed
if ! dpkg -l | grep -qw ufw; then
    echo "Installing ufw..."
    sudo apt update -y
    sudo apt install ufw -y
fi

# Process each service parameter
for SERVICE in "$@"; do
    case "$SERVICE" in
        RDP)
            echo "Configuring firewall rules for RDP..."
            if ! rule_exists "3389"; then
                sudo ufw allow from 10.0.0.0/8 to any port 3389
                sudo ufw allow from 172.16.0.0/12 to any port 3389
                sudo ufw allow from 192.168.0.0/16 to any port 3389
            else
                echo "RDP rules already exist."
            fi
            ;;
        SSH)
            echo "Configuring firewall rules for SSH..."
            if ! rule_exists "22"; then
                sudo ufw allow from 10.0.0.0/8 to any port 22 proto tcp
                sudo ufw allow from 172.16.0.0/12 to any port 22 proto tcp
                sudo ufw allow from 192.168.0.0/16 to any port 22 proto tcp
            else
                echo "SSH rules already exist."
            fi
            ;;
        *)
            echo "Invalid service specified: $SERVICE"
            echo "Usage: $0 {RDP|SSH} [RDP SSH]"
            exit 1
            ;;
    esac
done

# Enable UFW firewall, which may interrupt existing SSH sessions if rules are incorrect
echo "Enabling UFW firewall..."
sudo ufw --force enable

# Reload UFW (optional for the first configuration)
sudo ufw reload