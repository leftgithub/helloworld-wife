#!/bin/bash

# Ensure the script is run with sudo
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root (using sudo)." >&2
  exit 1
fi

# Get the username to configure
read -p "Enter the username to configure for passwordless apt commands: " USERNAME

# Check if the user exists
if ! id "$USERNAME" &>/dev/null; then
  echo "User '$USERNAME' does not exist. Exiting." >&2
  exit 1
fi

# Define the sudoers rule
SUDOERS_RULE="$USERNAME ALL=(ALL) NOPASSWD: /usr/bin/apt update, /usr/bin/apt upgrade"

# Check if the rule already exists
if sudo grep -Fxq "$SUDOERS_RULE" /etc/sudoers; then
  echo "Passwordless sudo for apt commands is already configured for $USERNAME."
else
  # Append the rule to the sudoers file
  echo "$SUDOERS_RULE" | sudo tee -a /etc/sudoers >/dev/null
  if [[ $? -eq 0 ]]; then
    echo "Passwordless sudo for apt commands has been configured for $USERNAME."
  else
    echo "Failed to update the sudoers file. Exiting." >&2
    exit 1
  fi
fi
