#If this is a new Alertmanager installation, we recommend enabling UTF-8 strict mode before creating an Alertmanager configuration file. You can enable strict mode with alertmanager --config.file=config.yml --enable-feature="utf8-strict-mode".
cd /tmp
wget https://github.com/prometheus/alertmanager/releases/download/v0.27.0/alertmanager-0.27.0.linux-amd64.tar.gz
tar -xvzf alertmanager-0.27.0.linux-amd64.tar.gz

sudo mv alertmanager-0.27.0.linux-amd64/alertmanager /usr/local/bin/
sudo mv alertmanager-0.27.0.linux-amd64/amtool /usr/local/bin/

#Create a Systemd Service for Alertmanager, storage.path is absolutely necessary and caused an hour of trouble-shooting
sudo nano /etc/systemd/system/alertmanager.service
[Unit]
Description=Alertmanager
After=network.target

[Service]
ExecStart=/usr/local/bin/alertmanager --config.file=/etc/alertmanager/alertmanager.yml --storage.path=/var/lib/alertmanager/data
Restart=always
User=alertmanager
Group=alertmanager

[Install]
WantedBy=multi-user.target
#end of what needs to be pasted and saved...

#Create the alertmanager User and Group:
sudo useradd --no-create-home --shell /bin/false alertmanager
sudo mkdir -p /etc/alertmanager
sudo chown -R alertmanager:alertmanager /etc/alertmanager

#failing without these steps
sudo mkdir -p /etc/alertmanager/data
sudo chown -R alertmanager:alertmanager /etc/alertmanager/data
sudo chmod 700 /etc/alertmanager/data

sudo mkdir -p /var/lib/alertmanager/data
sudo chown -R alertmanager:alertmanager /var/lib/alertmanager/data
sudo chmod 700 /var/lib/alertmanager/data


#Start and Enable the Service
sudo systemctl daemon-reload
sudo systemctl start alertmanager
sudo systemctl enable alertmanager


#alertmanager --config.file=config.yml --enable-feature="utf8-strict-mode"
sudo mkdir -p /etc/alertmanager
sudo nano /etc/alertmanager/alertmanager.yml
#enter and modify the following
global:
  resolve_timeout: 5m

route:
  group_by: ['alertname']
  receiver: 'email'

receivers:
  - name: 'email'
    email_configs:
      - to: 'your-email@example.com'
        from: 'alertmanager@example.com'
        smarthost: 'smtp.example.com:587'
        auth_username: 'your-smtp-username'
        auth_password: 'your-smtp-password'
        require_tls: true

#verify configuration
alertmanager --config.file=/etc/alertmanager/alertmanager.yml --enable-feature="utf8-strict-mode"
sudo systemctl restart alertmanager

#also need to tweak prometheus.yml
#sudo nano /etc/prometheus/prometheus.yml
#for example it might need a block like that below:
alerting:
  alertmanagers:
    - static_configs:
        - targets:
          # - alertmanager:9093
            - localhost:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"
    - "alert_rules.yml"
