#!/bin/bash

# Define the directory path
DIR="/tmp/scripts"

# Check if the directory exists
if [ ! -d "$DIR" ]; then
  echo "Directory $DIR does not exist. Creating it..."
  mkdir -p "$DIR"  # Create the directory, including any necessary parent directories
else
  echo "Directory $DIR already exists."
fi

# Change to the directory
cd "$DIR" || { echo "Failed to change directory to $DIR"; exit 1; }

sudo apt install wget dos2unix -y
wget "https://helloworld-wife-leftgithub-6685091628370c591e21e77a39229ff4c0a0.gitlab.io/assets/sh/urls.txt"
sudo dos2unix *.txt
wget -i urls.txt
sudo dos2unix *.txt
sudo dos2unix *.sh
sudo chmod a+x *.sh
#for file in install_desktop_environment.sh configure_firewall_rules.sh configure_xrdp.sh detect_environment.sh download_scripts.sh generate_console_banner.sh generate_ssh_banner.sh hello_world.sh update_rdp_banner.sh remove_scripts.sh; do
#    rm -f "$file"
#    wget "https://helloworld-wife-leftgithub-6685091628370c591e21e77a39229ff4c0a0.gitlab.io/assets/sh/$file"
#    sudo chmod a+x "$file"
#    sudo dos2unix "$file"
#done