#install curl
sudo apt install curl -y

#Register GPG Key
curl -fsSL https://miktex.org/download/key | sudo tee /usr/share/keyrings/miktex-keyring.asc > /dev/null

#Register installation source
echo "deb [signed-by=/usr/share/keyrings/miktex-keyring.asc] https://miktex.org/download/debian bookworm universe" | sudo tee /etc/apt/sources.list.d/miktex.list

#Install MiKTeX
sudo apt-get update
sudo apt-get install miktex -y


#configure shared installation with automatic package installation enabled
sudo miktexsetup --shared=yes finish
sudo initexmf --admin --set-config-value [MPM]AutoInstall=1


#manually installed VS Code

#installing nginx
sudo apt update
sudo apt install nginx

#Set Up the Nginx Configuration
sudo nano /etc/nginx/sites-available/latex

#Enable the site
sudo ln -s /etc/nginx/sites-available/latex /etc/nginx/sites-enabled/

#Adjust Permissions
sudo chmod o+rx /home /home/myusername /home/myusername/Documents
sudo chmod -R o+r /home/myusername/Documents/Latex/*

#Test Nginx
sudo nginx -t
sudo systemctl restart nginx

#check that nginx is enabled
sudo systemctl is-enabled nginx
