#run this after installing node_exporter on each linux host

#configure service checks for prometheus monitoring
if [ ! -d /var/lib/node_exporter/textfile_collector ]; then
    sudo mkdir -p /var/lib/node_exporter/textfile_collector
    sudo chown vboxuser /var/lib/node_exporter/textfile_collector
    sudo sed -i 's|^ExecStart=.*|ExecStart=/usr/local/bin/node_exporter --collector.textfile.directory=/var/lib/node_exporter/textfile_collector|' /etc/systemd/system/node_exporter.service
fi

echo '#!/bin/bash

# Define the output file for the Prometheus metrics
OUTPUT_FILE="/var/lib/node_exporter/textfile_collector/mssql-server_status.prom"

# Check the status of the mssql-server service, note that hyphen is not allowed in prometheus metric names
if systemctl is-active --quiet mssql-server; then
    echo "mssql_server_service_status 1" > "$OUTPUT_FILE"
else
    echo "mssql_server_service_status 0" > "$OUTPUT_FILE"
fi' > /var/lib/node_exporter/textfile_collector/mssql_server_check.sh && chmod +x /var/lib/node_exporter/textfile_collector/mssql-server_check.sh

CRON_JOB="* * * * * /var/lib/node_exporter/textfile_collector/mssql-server_check.sh"

# Check if the cron job already exists
(crontab -l 2>/dev/null | grep -F "$CRON_JOB") || {
    # If not, add the new cron job
    (crontab -l 2>/dev/null; echo "$CRON_JOB") | crontab -
    echo "Cron job added: $CRON_JOB"
}


sudo systemctl daemon-reload
sudo systemctl restart node_exporter