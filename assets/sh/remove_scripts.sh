#!/bin/bash

# Define the directory path
DIR="/tmp/scripts"

# Check if the directory exists
if [ -d "$DIR" ]; then
  rm $DIR/*.*
  rmdir $DIR
fi
