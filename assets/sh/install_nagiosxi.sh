wget https://repo.nagios.com/GPG-KEY-NAGIOS-V3
sudo apt-get install -y rpm curl
sudo ufw allow from 10.0.0.0/8 to any port 80
sudo ufw allow from 172.16.0.0/12 to any port 80
sudo ufw allow from 192.168.0.0/16 to any port 80
rpm --import GPG-KEY-NAGIOS-V3
su -
curl https://assets.nagios.com/downloads/nagiosxi/install.sh | sh