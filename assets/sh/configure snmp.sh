sudo apt update
sudo apt install -y snmpd

sudo nano /etc/snmp/snmpd.conf
#Look for the line: rocommunity public  default    -V systemonly
#You can change "public" to a more secure string if you prefer.
#If the string you're using includes special characters like !, you can escape it using a backslash (\): \!
#the sting also can be enclosed with single quotes

#sudo ufw allow from <OpManager_IP> to any port 161
sudo ufw allow from 10.0.1.29 to any port 161
sudo ufw reload

sudo systemctl restart snmpd
sudo systemctl enable snmpd
