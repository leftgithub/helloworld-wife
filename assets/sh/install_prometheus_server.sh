sudo apt update && sudo apt upgrade -y

#Create a User for Prometheus
sudo useradd --no-create-home --shell /bin/false prometheus

#Download Prometheus
cd /tmp
wget https://github.com/prometheus/prometheus/releases/download/v3.1.0-rc.0/prometheus-3.1.0-rc.0.linux-amd64.tar.gz

#Extract and Move Files
tar -xvzf prometheus-3.1.0-rc.0.linux-amd64.tar.gz
cd prometheus-3.1.0-rc.0.linux-amd64

sudo mv prometheus /usr/local/bin/
sudo mv promtool /usr/local/bin/
sudo mkdir -p /etc/prometheus
sudo mv prometheus.yml /etc/prometheus/

#Set Permissions
sudo chown -R prometheus:prometheus /etc/prometheus
sudo chown prometheus:prometheus /usr/local/bin/prometheus
sudo chown prometheus:prometheus /usr/local/bin/promtool

#Create a Systemd Service
sudo nano /etc/systemd/system/prometheus.service

#add the following comamnds:
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/var/lib/prometheus/ \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target

#Create Data Directory
sudo mkdir /var/lib/prometheus
sudo chown prometheus:prometheus /var/lib/prometheus

#Reload Systemd and Start Prometheus
sudo systemctl daemon-reload
sudo systemctl start prometheus
sudo systemctl enable prometheus

#Verify Installation
sudo systemctl status prometheus
#You can also access the Prometheus web interface at: http://<your_server_ip>:9090

#need to download node_exporter on the server
wget https://github.com/prometheus/node_exporter/releases/download/v1.8.2/node_exporter-1.8.2.linux-amd64.tar.gz
tar -xvzf node_exporter-1.8.2.linux-amd64.tar.gz
cd node_exporter-1.8.2.linux-amd64
sudo mv node_exporter /usr/local/bin/
sudo useradd -rs /bin/false node_exporter
sudo nano /etc/systemd/system/node_exporter.service
#with the content below:
'[Unit]
Description=Prometheus Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
'
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter


#need to update settings in /etc/prometheus/prometheus.yml
#need to download node_exporter and windows_exporter on clients
#for windows need to download the exporter from https://github.com/prometheus-community/windows_exporter/releases
