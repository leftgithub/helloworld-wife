wget https://github.com/prometheus/node_exporter/releases/download/v1.8.2/node_exporter-1.8.2.linux-amd64.tar.gz
tar -xvzf node_exporter-1.8.2.linux-amd64.tar.gz
cd node_exporter-1.8.2.linux-amd64
sudo mv node_exporter /usr/local/bin/
sudo useradd -rs /bin/false node_exporter
sudo nano /etc/systemd/system/node_exporter.service
#with the content below:
'[Unit]
Description=Prometheus Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
'
#need to add allow on the firewall
sudo ufw allow from 10.0.1.167 to any port 9100 proto tcp
#sudo ufw allow from 10.0.0.0/8 to any port 9100 proto tcp
#sudo ufw allow from 172.16.0.0/12 to any port 9100 proto tcp
#sudo ufw allow from 192.168.0.0/16 to any port 9100 proto tcp

sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter

#run this after installing node_exporter on each linux host

#configure service checks for prometheus monitoring
#use a previously created dedicated user named node_exporter, or $USER
if [ ! -d /var/lib/node_exporter/textfile_collector ]; then
    sudo mkdir -p /var/lib/node_exporter/textfile_collector
    sudo chown node_exporter /var/lib/node_exporter/textfile_collector
    sudo sed -i 's|^ExecStart=.*|ExecStart=/usr/local/bin/node_exporter --collector.textfile.directory=/var/lib/node_exporter/textfile_collector|' /etc/systemd/system/node_exporter.service
fi

sudo bash -c 'echo "#!/bin/bash

# Define the output file for the Prometheus metrics
OUTPUT_FILE=\"/var/lib/node_exporter/textfile_collector/ntp_status.prom\"

# Check the status of the NTP service
if systemctl is-active --quiet ntp; then
    echo \"ntp_service_status 1\" > \$OUTPUT_FILE
else
    echo \"ntp_service_status 0\" > \$OUTPUT_FILE
fi" > /var/lib/node_exporter/textfile_collector/ntp_check.sh'

sudo chmod +x /var/lib/node_exporter/textfile_collector/ntp_check.sh

#sudo chown node_exporter:$USER /var/lib/node_exporter/textfile_collector
#sudo chmod 755 /var/lib/node_exporter/textfile_collector

CRON_JOB="* * * * * root /var/lib/node_exporter/textfile_collector/ntp_check.sh"

# Check if the cron job already exists in /etc/crontab
grep -F "$CRON_JOB" /etc/crontab || {
    # If not, add the new cron job to /etc/crontab
    echo "$CRON_JOB" | sudo tee -a /etc/crontab > /dev/null
    echo "Cron job added: $CRON_JOB"
}


#problem is that this evaluates the time zone only when the script is created
#so you may need a script to create this, give it execute, and run it...
#sudo bash -c 'echo "#!/bin/bash" > /var/lib/node_exporter/textfile_collector/timezone_check.sh; echo "echo \"timezone_info#{name=\\\"$(cat /etc/timezone)\\\"} 1\" > /var/lib/node_exporter/textfile_collector/timezone.prom" >> /var/lib/node_exporter/textfile_collector/timezone_check.sh'

#sudo chmod +x /var/lib/node_exporter/textfile_collector/timezone_check.sh
#sudo sh /var/lib/node_exporter/textfile_collector/timezone_check.sh

#sudo crontab -e
#schedule once per day at midnight
#0 0 * * * /var/lib/node_exporter/textfile_collector/timezone_check.sh

#these chown command may or may not be necessary
#sudo chown -R node_exporter:node_exporter /var/lib/node_exporter/textfile_collector
#sudo chmod 755 /var/lib/node_exporter/textfile_collector
#sudo chown node_exporter:node_exporter /var/lib/node_exporter/textfile_collector/timezone.prom
#sudo chmod 644 /var/lib/node_exporter/textfile_collector/timezone.prom



sudo systemctl daemon-reload
sudo systemctl restart node_exporter
