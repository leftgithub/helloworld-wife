#https://www.howtoforge.com/how-to-install-microsoft-sql-server-on-debian-12/

#Now install package dependencies using the following command
sudo apt install -y gnupg2 apt-transport-https wget curl

#After package dependencies are installed, add the GPG key for the MS SQL Server repository by running the command below.
wget -q -O- https://packages.microsoft.com/keys/microsoft.asc | \
gpg --dearmor | sudo tee /usr/share/keyrings/microsoft.gpg > /dev/null 2>&1

#Then, add the MS SQL Server repository with the command below. In this guide, you will install the MS SQL Server 2022.
echo "deb [signed-by=/usr/share/keyrings/microsoft.gpg arch=amd64,armhf,arm64] https://packages.microsoft.com/ubuntu/22.04/mssql-server-2022 jammy main" | \
sudo tee /etc/apt/sources.list.d/mssql-server-2022.list

#After adding the GPG key and repository of MS SQL Server, use the command below to refresh your Debian repository and retrieve package information for MS SQL Server.
sudo apt update

#Install the MS SQL Server 2022 using the apt command below
sudo apt install -y mssql-server

#Run the command below to complete the MS SQL Server installation, and create max 128 character password
sudo /opt/mssql/bin/mssql-conf setup


#update firewall rules
sudo ufw allow from 192.168.0.0/16 to any port 1433 proto tcp
sudo ufw reload

#configure sql server agent
sudo /opt/mssql/bin/mssql-conf set sqlagent.enabled true
sudo systemctl restart mssql-server

#copy .bak files with scp, winscp, or something else
#move them to the default directory that sql server looks for backups
sudo mv *.bak /var/opt/mssql/data
#trying to figure out how to restore backup from a remote sql server instance
sudo chown mssql:mssql /var/opt/mssql/data

#NOT ABLE TO RESTORE DATABASES using the GUI in sql mangement studio, instead need to run commands as a dynamic query
#example database restore commands run from sql management studio as opposed to Azure Data Studio since I am using headless server
RESTORE DATABASE db_SqlMonitor
FROM DISK = '/var/opt/mssql/data/dbSqlMonitor.bak'
WITH MOVE 'db_SqlMonitor' TO '/var/opt/mssql/data/db_SqlMonitor.mdf',
     MOVE 'db_SqlMonitor_log' TO '/var/opt/mssql/data/db_SqlMonitor_log.ldf',
     REPLACE;

RESTORE DATABASE PA
FROM DISK = '/var/opt/mssql/data/PA.bak'
WITH MOVE 'PA' TO '/var/opt/mssql/data/PA.mdf',
     MOVE 'PA_log' TO '/var/opt/mssql/data/PA_log.ldf',
     REPLACE;

#make directories to read and write files
sudo mkdir /usr/share/ReadAccessSqlServer
sudo mkdir /usr/share/WriteAccessSqlServer
sudo chown mssql:mssql /usr/share/ReadAccessSqlServer
sudo chown mssql:mssql /usr/share/WriteAccessSqlServer

#unfortunately, xpcmdshell is not allowed in sql server on linux, so cannot write query results from a stored procedure
#trying to install bcp as a workaround rather than writing CLR integration functions as a workaround
echo "deb [signed-by=/usr/share/keyrings/microsoft.gpg arch=amd64,armhf,arm64] https://packages.microsoft.com/ubuntu/22.04/prod jammy main" | \
sudo tee /etc/apt/sources.list.d/prod.list
sudo apt update
sudo apt install -y mssql-tools unixodbc-dev
export PATH="$PATH:/opt/mssql-tools/bin"
source ~/.bashrc

#echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> /etc/environment
#source /etc/environment
#echo $PATH
#which sqlcmd
#which bcp

