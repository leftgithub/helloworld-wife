#!/bin/bash

# Check if mode parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 {Development|Staging|Production}"
  exit 1
fi

# Get the mode (banner text) from the command line argument
#BANNER_TEXT=$1
# Capture all arguments as a single string
BANNER_TEXT="$*"

# Use `printf` to create the banner text with hostname
BANNER_TEXT_WITH_HOSTNAME=$(printf "%s\n%s" "$BANNER_TEXT" "$(hostname)")

# Set a multi-line banner in /etc/issue
# -a is for append instead of overwrite
echo "$BANNER_TEXT_WITH_HOSTNAME" | sudo tee -a /etc/issue > /dev/null
echo "$BANNER_TEXT_WITH_HOSTNAME" | sudo tee -a /etc/issue.net > /dev/null