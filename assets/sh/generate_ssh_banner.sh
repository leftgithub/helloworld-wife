#!/bin/bash
sudo apt install figlet lolcat -y

# Check if mode parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 {Development|Staging|Production}"
  exit 1
fi

# Get the mode (banner text) from the command line argument
#BANNER_TEXT=$1
# Capture all arguments as a single string
BANNER_TEXT="$1"

# Full paths to figlet and lolcat
FIGLET_CMD="/usr/bin/figlet"
LOLCAT_CMD="/usr/games/lolcat"

# Generate banner with figlet and lolcat
#BANNER=$("$FIGLET_CMD" "$BANNER_TEXT $(hostname)" -c | "$LOLCAT_CMD")
# Use figlet to generate the banner with text split into two lines
BANNER=$(printf "%s\n%s" "$BANNER_TEXT" "$(hostname)" | "$FIGLET_CMD" -c | "$LOLCAT_CMD")

# Save banner to /etc/motd
echo "$BANNER" | sudo tee /etc/motd > /dev/null

# Ensure SSH configuration file exists
if [ ! -f /etc/ssh/sshd_config ]; then
  echo "SSH configuration file /etc/ssh/sshd_config does not exist."
  exit 1
fi

# Ensure the PrintMotd option is set to yes
grep -q '^PrintMotd yes' /etc/ssh/sshd_config || echo 'PrintMotd yes' | sudo tee -a /etc/ssh/sshd_config > /dev/null

# Restart SSH service to apply changes
sudo systemctl restart ssh
