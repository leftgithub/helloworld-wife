#!/bin/bash

# Check if mode parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 {Development|Staging|Production}"
  exit 1
fi

# Get the mode (banner text) from the command line argument
#BANNER_TEXT=$1
# Capture all arguments as a single string
BANNER_TEXT="$*"

# Use `printf` to create the banner text with hostname
BANNER_TEXT_WITH_HOSTNAME=$(printf "%s\n%s" "$BANNER_TEXT" "$(hostname)")

# Escape any slashes or special characters in the banner text for sed
ESCAPED_BANNER_TEXT=$(echo "$BANNER_TEXT_WITH_HOSTNAME" | sed 's/[&/\]/\\&/g')

# Path to the xrdp.ini file
XRDP_INI_FILE="/etc/xrdp/xrdp.ini"

# Check if the xrdp.ini file exists
if [ ! -f "$XRDP_INI_FILE" ]; then
  echo "Error: xrdp.ini file not found at $XRDP_INI_FILE"
  exit 1
fi

# Update or add the banner line in the xrdp.ini file
sudo sed -i "/^banner=/c\banner=$ESCAPED_BANNER_TEXT" "$XRDP_INI_FILE"

# If the line does not exist, add it
if ! grep -q "^banner=" "$XRDP_INI_FILE"; then
  echo "banner=$ESCAPED_BANNER_TEXT" | sudo tee -a "$XRDP_INI_FILE" > /dev/null
fi

# Restart xrdp service to apply changes
sudo systemctl restart xrdp

echo "RDP banner updated successfully."