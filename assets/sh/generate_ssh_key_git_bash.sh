#git bash
ssh-keygen -t ed25519 -C "your_email@example.com"

#In a new admin elevated PowerShell window, ensure the ssh-agent is running. You can use the "Auto-launching the ssh-agent" instructions in "Working with SSH key passphrases", or start it manually:

# start the ssh-agent in the background
#Get-Service -Name ssh-agent | Set-Service -StartupType Manual
#Start-Service ssh-agent

#In a terminal window without elevated permissions, add your SSH private key to the ssh-agent. If you created your key with a different name, or if you are adding an existing key that has a different name, replace id_ed25519 in the command with the name of your private key file.

#ssh-add c:/Users/%username%/.ssh/id_ed25519

#Add the SSH public key to your account on GitHub. For more information, see "Adding a new SSH key to your GitHub account." (https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account)

#Copy the SSH public key to your clipboard in git bash
#clip < ~/.ssh/id_ed25519.pub
#cat ~/.ssh/id_ed25519.pub | clip

#In the upper-right corner of any page on GitHub, click your profile photo, then click Settings.

#In the "Access" section of the sidebar, click  SSH and GPG keys.

#Click New SSH key or Add SSH key.

#Need to create .ppk file from id_ed25519 using puttygen for use with putty on windows