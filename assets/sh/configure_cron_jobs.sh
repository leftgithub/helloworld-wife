#!/bin/bash
#precondition: chmod +x configure_cron_jobs.sh

# Define the cron job
CRON_JOB="0 2 * * 6 /usr/local/bin/update-upgrade.sh"

# Check if the cron job already exists
(crontab -l 2>/dev/null | grep -F "$CRON_JOB") || {
    # If not, add the new cron job
    (crontab -l 2>/dev/null; echo "$CRON_JOB") | crontab -
    echo "Cron job added: $CRON_JOB"
}
