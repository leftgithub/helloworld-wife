#not electing to use a proxy such as Zyte due to cost and/or safety and stability of using free proxies
#not electing to utilize temporal

#install selenium, bs4/beautifulsoup, requests, temporal, pyodbc
#pip install selenium webdriver-manager lxml

sudo apt update
sudo apt upgrade -y
sudo apt install -y python3
sudo apt install -y python3-pip #need unixodbc and unixodbc-dev for pyodbc to work properly

#create virtual environment
sudo apt install -y python3-venv  # Install venv if it's not already installed
python3 -m venv webscraper_env
source webscraper_env/bin/activate
sudo apt install -y unixodbc unixodbc-dev #need unixodbc and unixodbc-dev for pyodbc to work properly
pip install --upgrade pip setuptools
pip install bs4 selenium webdriver-manager pyodbc lxml

#install odbc driver for sql server
#https://learn.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver16&tabs=alpine18-install%2Calpine17-install%2Cdebian8-install%2Credhat7-13-install%2Crhel7-offline

sudo apt install -y curl

# Debian 12
curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --dearmor -o /usr/share/keyrings/microsoft-prod.gpg

#Download appropriate package for the OS version
#Choose only ONE of the following, corresponding to your OS version

#Debian 12
curl https://packages.microsoft.com/config/debian/12/prod.list | sudo tee /etc/apt/sources.list.d/mssql-release.list

sudo apt-get update
sudo ACCEPT_EULA=Y apt-get install -y msodbcsql18
# optional: for bcp and sqlcmd
sudo ACCEPT_EULA=Y apt-get install -y mssql-tools18
echo 'export PATH="$PATH:/opt/mssql-tools18/bin"' >> ~/.bashrc
source ~/.bashrc
# optional: for unixODBC development headers
sudo apt-get install -y unixodbc-dev
# optional: kerberos library for debian-slim distributions
#sudo apt-get install -y libgssapi-krb5-2