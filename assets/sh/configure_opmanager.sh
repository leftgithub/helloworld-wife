#https://www.manageengine.com/network-monitoring/29809517/ManageEngine_OpManager_64bit.bin
wget https://www.manageengine.com/network-monitoring/29809517/ManageEngine_OpManager_64bit.bin
chmod +x ManageEngine_OpManager_64bit.bin
sudo ./ManageEngine_OpManager_64bit.bin
rm ManageEngine_OpManager_64bit.bin

#8060 used for http, 8061 used for https
sudo ufw allow 8060/tcp
sudo ufw allow 8061/tcp
cd /opt/ManageEngine/OpManager/bin/
sudo sh /opt/ManageEngine/OpManager/bin/linkAsService.sh
sudo systemctl start OpManager.service
sudo systemctl enable OpManager.service
#Access the Web Interface: After the installation is complete and the service is running, you can access the OpManager web interface:
#default credentials are admin / admin
#http://<your-server-ip>:8060

#optional application plug uses ports 9090, 8443

#In windows, need to configure SNMP Service and SNMP Trap manually or using group policy
#To configure SNMP, open the SNMP Service Properties:
#Go to Control Panel > Administrative Tools > Services.
#Locate and double-click on SNMP Service.
#Under the Agent tab, ensure that the Contact and Location fields are filled if needed.
#Under the Security tab, you need to:
#Accept SNMP packets from these hosts: Add the IP address of the OpManager server to this list to allow it to query the system.
#Community Strings: Configure a read-only or read-write community string (for SNMPv2, this is similar to a password to access SNMP data)#. For example, set a community string like public for read-only access.
#Add a community string: public (or any string you prefer).
#Set the Access Type to Read Only or Read Write depending on your needs.
#If you don't see the agent tab, may need to go to server manager and install the wmi provider for the SNMP Service feature

#wound up having to find these registry keys:
#Computer\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SNMP\Parameters\ValidCommunities
#Computer\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SNMP\Parameters\PermittedManagers
#created DWORD under ValidCommunities with value 4 and name of the community string
#created REG_SZ under PermittedManagers with value of 2 and ipaddress of opmanager server

#Windows Powershell
#rem for windows server
#Enable-WindowsOptionalFeature -Online -FeatureName SNMP -All
#rem for windows 10/11
#Add-WindowsCapability -Online -Name SNMP.Client~~~~0.0.1.0


#New-NetFirewallRule -Protocol UDP -LocalPort 161 -Action Allow -Name "Allow SNMP"
#New-NetFirewallRule -Protocol UDP -LocalPort 162 -Action Allow -Name "Allow SNMP Traps"

