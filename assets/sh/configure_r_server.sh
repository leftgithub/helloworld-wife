sudo apt update
sudo apt install -y r-base r-base-dev
sudo apt-get install -y gdebi-core
wget https://download2.rstudio.org/server/jammy/amd64/rstudio-server-2024.09.1-394-amd64.deb
sudo gdebi rstudio-server-2024.09.1-394-amd64.deb
sudo ufw allow from 10.0.0.0/8 to any port 8787
sudo ufw allow from 172.16.0.0/12 to any port 8787
sudo ufw allow from 192.168.0.0/16 to any port 8787
sudo ufw reload
#in order to install R package curl
sudo apt install -y libcurl4-openssl-dev
#need to download install_package.R
#need to download custom R package, and then run
Rscript install_package.R

#in order to install the odbc R package:
sudo apt-get install unixodbc-dev
#need to install libxml2-dev prior to install roxygen for rcpp package development: sudo apt-get install libxml2-dev
sudo apt-get install libxml2-dev
#need to install roxygen for rcpp package: install.packages('roxygen2', dependences=TRUE)

#to avoid warnings about pdflatex not being available while checking packages
sudo apt-get install -y texlive
sudo apt-get install -y texlive-fonts-extra


#Now install package dependencies using the following command
sudo apt install -y gnupg2 apt-transport-https wget curl

#After package dependencies are installed, add the GPG key for the MS SQL Server repository by running the command below.
wget -q -O- https://packages.microsoft.com/keys/microsoft.asc | \
gpg --dearmor | sudo tee /usr/share/keyrings/microsoft.gpg > /dev/null 2>&1

#Then, add the MS SQL Server repository with the command below. In this guide, you will install the MS SQL Server 2022.
echo "deb [signed-by=/usr/share/keyrings/microsoft.gpg arch=amd64,armhf,arm64] https://packages.microsoft.com/ubuntu/22.04/mssql-server-2022 jammy main" | \
sudo tee /etc/apt/sources.list.d/mssql-server-2022.list



sudo apt update
#sudo apt install msodbcsql17
#since line above did not work, used 3 lines below
wget https://packages.microsoft.com/debian/10/prod/pool/main/m/msodbcsql17/msodbcsql17_17.10.2.1-1_amd64.deb
sudo apt-get install -y unixodbc odbcinst
sudo dpkg -i msodbcsql17_17.10.2.1-1_amd64.deb


