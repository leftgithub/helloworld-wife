#!/bin/bash
#wget https://helloworld-wife-leftgithub-6685091628370c591e21e77a39229ff4c0a0.gitlab.io/assets/sh/change_host_name.sh
#sudo dos2unix change_host_name.sh
#sudo chmod a+x change_host_name.sh


sudo nano /etc/hostname
sudo nano /etc/hosts
sudo hostnamectl set-hostname new-hostname
#if the above line fails, may need to run: sudo systemctl start dbus
sudo systemctl restart systemd-hostnamed


sudo nano /etc/network/interfaces
#change ipaddress and on router/firewall level create ipadress reservations
sudo systemctl restart networking

#may also want to edit zone files in /etc/bind/ on dns server
#may also want to run code based on environment: production, staging, development
#may also want to edit users

exit

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root or use sudo."
  exit 1
fi

# Check if a hostname parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 new-hostname"
  exit 1
fi

NEW_HOSTNAME="$1"

# Ensure the hostname is valid (only letters, numbers, hyphens, and periods)
if ! echo "$NEW_HOSTNAME" | grep -E '^([a-zA-Z0-9-]{1,63})$'; then
  echo "Invalid hostname. Hostnames can only contain letters, numbers, and hyphens."
  exit 1
fi

# Update /etc/hostname (does not work properly)
sudo echo "$NEW_HOSTNAME" > /etc/hostname

# Update /etc/hosts
# Backup the current /etc/hosts
sudo cp /etc/hosts /etc/hosts.bak

# Replace old hostname with the new hostname in /etc/hosts
# This will handle cases where the old hostname is not just a simple word, but may be longer
sudo sed -i "s/\b$(hostname)\b/$NEW_HOSTNAME/g" /etc/hosts

# Update the hostname immediately
sudo hostname "$NEW_HOSTNAME"

# Optionally, restart systemd-logind to apply changes without reboot
sudo systemctl restart systemd-logind

# Inform the user
echo "Hostname changed to $NEW_HOSTNAME. Please reboot the system for changes to take full effect."

