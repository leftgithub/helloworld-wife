sudo apt update -y
sudo apt install lxqt -y

rm ai-8420370_1280.jpg
rm polynesia.jpg
rm robots.jpg
#cannot handle .webp file
wget https://gitlab.com/leftgithub/helloworld-wife/-/raw/master/assets/img/ai-8420370_1280.jpg
wget https://gitlab.com/leftgithub/helloworld-wife/-/raw/master/assets/img/polynesia.jpg
wget https://gitlab.com/leftgithub/helloworld-wife/-/raw/master/assets/img/robots.jpg
sudo mv ai-8420370_1280.jpg /usr/share/lxqt/themes/debian/
sudo mv polynesia.jpg /usr/share/lxqt/themes/debian/
sudo mv robots.jpg /usr/share/lxqt/themes/debian/

# Check if ENVIRONMENT parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 {Development|Staging|Production}"
  exit 1
fi
ENVIRONMENT="$1" #use $* for all and $1 for the first argument only

#want functionality like gbinfo on windows for ubuntu https://learn.microsoft.com/en-us/sysinternals/downloads/bginfo
case "$ENVIRONMENT" in
    Development)
        # Path to the new wallpaper
        WALLPAPER_PATH="/usr/share/lxqt/themes/debian/polynesia.jpg"
        ;;
    Staging)
        # Path to the new wallpaper
        WALLPAPER_PATH="/usr/share/lxqt/themes/debian/robots.jpg"
        ;;
    Production)
        # Path to the new wallpaper
        WALLPAPER_PATH="/usr/share/lxqt/themes/debian/ai-8420370_1280.jpg"
        ;;
    *)
        echo "Usage: $0 {Development|Staging|Production}"
        exit 1
        ;;
esac
#pcmanfm-qt --help #to see options
echo "In RDP session, please run: pcmanfm-qt -w $WALLPAPER_PATH --wallpaper-mode center"