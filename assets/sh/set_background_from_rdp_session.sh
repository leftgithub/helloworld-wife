# Get the IP addresses of the machine
IP_ADDRESSES=$(hostname -I)
ENVIRONMENT="Unknown"

# Check each IP address
for IP_ADDRESS in $IP_ADDRESSES; do
    # Extract the first three octets of the IP address
    OCT1=$(echo "$IP_ADDRESS" | cut -d. -f1)
    OCT2=$(echo "$IP_ADDRESS" | cut -d. -f2)
    OCT3=$(echo "$IP_ADDRESS" | cut -d. -f3)

    # Determine the environment based on the IP address
    if [ "$OCT1" -eq 192 ] && [ "$OCT2" -eq 168 ]; then
        if [ "$OCT3" -le 79 ]; then
            ENVIRONMENT="Development"
            break
        elif [ "$OCT3" -ge 80 ] && [ "$OCT3" -le 99 ]; then
            ENVIRONMENT="Staging"
            break
        elif [ "$OCT3" -ge 100 ] && [ "$OCT3" -le 255 ]; then
            ENVIRONMENT="Production"
            break
        fi
    fi
done

#want functionality like gbinfo on windows for ubuntu https://learn.microsoft.com/en-us/sysinternals/downloads/bginfo
case "$ENVIRONMENT" in
    Development)
        # Path to the new wallpaper
        WALLPAPER_PATH="/usr/share/lxqt/themes/debian/polynesia.jpg"
        ;;
    Staging)
        # Path to the new wallpaper
        WALLPAPER_PATH="/usr/share/lxqt/themes/debian/robots.jpg"
        ;;
    Production)
        # Path to the new wallpaper
        WALLPAPER_PATH="/usr/share/lxqt/themes/debian/ai-8420370_1280.jpg"
        ;;
    *)
        echo "Usage: $0 {Development|Staging|Production}"
        exit 1
        ;;
esac
#pcmanfm-qt --help #to see options
pcmanfm-qt -w $WALLPAPER_PATH --wallpaper-mode center
