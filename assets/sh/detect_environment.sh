#!/bin/bash

#private ipaddress subnets are as follows
#1) 10.0.0.0/8 (10.0.0.0 - 10.255.255.255)
#2) 172.16.0.0/12 (172.16.0.0 - 172.31.255.255)
#3) 192.168.0.0/16 (192.168.0.0 - 192.168.255.255)

#In VirtualBox
#1) NAT Network: By default, VirtualBox often uses the 10.0.2.0/24 subnet for its NAT network. This is a specific subnet within the 10.0.0.0/8 range.
# Host-Only Network: VirtualBox commonly uses 192.168.56.0/21 (192.168.56.0 - 192.168.63.255) for its host-only network. This is a subnet within the 192.168.0.0/16 range.

#home network
#host-only ethernet adapter uses 192.168.56.0/24 subnet
#wifi lan ip uses 192.168.68.1 with 255.255.252.0 subnet mask
#wifi dhcp assigns 192.168.68.50 - 192.168.71.250 with default gateway 192.168.68.1

#elections:
#1) to avoid having to setup static routes or other workaround, elect not to use 172.16.0.0/12
#2) elect not to use 10.0.0.0/8, which assume may be used by an enterprise LAN
#3) treat 192.168.0.0-192.168.79.255 as development
#4) treat 192.168.80.x through 192.168.99.x as staging
#5) treat 192.168.100.x through 192.168.255.x as production
#6) elect not to use other subnets including 


# Get the IP addresses of the machine
IP_ADDRESSES=$(hostname -I)
ENVIRONMENT="Unknown"

# Check each IP address
for IP_ADDRESS in $IP_ADDRESSES; do
    # Extract the first three octets of the IP address
    OCT1=$(echo "$IP_ADDRESS" | cut -d. -f1)
    OCT2=$(echo "$IP_ADDRESS" | cut -d. -f2)
    OCT3=$(echo "$IP_ADDRESS" | cut -d. -f3)

    # Determine the environment based on the IP address
    if [ "$OCT1" -eq 192 ] && [ "$OCT2" -eq 168 ]; then
        if [ "$OCT3" -le 79 ]; then
            ENVIRONMENT="Development"
            break
        elif [ "$OCT3" -ge 80 ] && [ "$OCT3" -le 99 ]; then
            ENVIRONMENT="Staging"
            break
        elif [ "$OCT3" -ge 100 ] && [ "$OCT3" -le 255 ]; then
            ENVIRONMENT="Production"
            break
        fi
    fi
done

# Output the result
echo "$ENVIRONMENT"