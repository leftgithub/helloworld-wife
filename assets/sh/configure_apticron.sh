#install and configure apticron
#light-weight patch management tool that checks for package updates and sends email notifications, allowing administrators to manually schedule updates as needed.
sudo apt install -y apticron
sudo cp /usr/lib/apticron/apticron.conf /etc/apticron

# Get the fully qualified domain name (FQDN)
fqdn=$(hostname -f)

# Extract the domain part from the FQDN
# The domain is the part after the first dot
domain=$(echo "$fqdn" | awk -F. '{ if (NF > 1) print substr($0, index($0, $2)); }')

#notify="apticron"
notify="$USER"

#where to send email notifications
#EMAIL="your-email@example.com"
# This setting controls which types of updates trigger an email. For example, you might only want to be notified of security updates:
sudo sed -i "s/^EMAIL=\"root\"/EMAIL=\"${notify}@${domain}\"/" /etc/apticron/apticron.conf

# Define the lines to be added
line1='EMAILREPORTS="security"'
line2='NOTIFYMAIL="yes"'
line3='DIST="stable"'

# Define the configuration file
config_file="/etc/apticron/apticron.conf"

# Function to add a line if it doesn't already exist
add_line_if_missing() {
    local line="$1"
    local file="$2"
    grep -qF "$line" "$file" || echo "$line" | sudo tee -a "$file" > /dev/null
}

# Add each line if it is missing
add_line_if_missing "$line1" "$config_file"
add_line_if_missing "$line2" "$config_file"
add_line_if_missing "$line3" "$config_file"
