sudo apt-get remove --purge lxqt-core
sudo apt-get remove --purge lxqt* lxqt-* liblxqt*
sudo apt-get autoremove
sudo apt-get autoclean
sudo rm -rf /etc/xdg/lxqt
sudo rm -rf /usr/share/lxqt
sudo rm -rf ~/.config/lxqt
sudo apt-get remove --purge sddm  # For SDDM, which appears to be the display manager causing problems
sudo reboot