#!/bin/bash
echo "To run this script: "
echo "1) sudo apt install openssh-server dos2unix wget -y"
echo "2) action: connect via putty using the virtual machine's static ipaddress once openssh-server is installed"
echo "3) comment: the reason is to able to copy and paste the code below with the long url"
echo "3) sudo apt install dos2unix wget -y"
echo "4) rm hello_world.sh"
echo "5) wget https://helloworld-wife-leftgithub-6685091628370c591e21e77a39229ff4c0a0.gitlab.io/hello_world.sh"
echo "6) dos2unix hello_world.sh"
echo "7) sudo chmod a+x hello_world.sh   #Gives everyone execute permissions"
echo "8) sudo sh hello_world.sh"
# VirtualBox Setup Instructions (manual steps)
echo "VirtualBox Setup Instructions:"
echo "1) Install VirtualBox and Oracle VirtualBox Extension Pack."
echo "2) Add Bridged Adapter under Network Adapters."
echo "3) Set promiscuous mode to 'Deny' under Network Adapters, which is the default"
echo "4) Go to Settings -> Display -> Remote Display -> Enable Server."
echo "5) Go to Settings -> Display -> Remote Display -> Allow Multiple Connection -> Check the Checkbox"
echo "6) Go to Settings -> Display -> Screen -> Maximize the Video Memory."
echo "7) Go to Settings -> General -> Advanced -> Shared Clipboard -> Host to Guest"
echo "to add vboxuser to the sudoers list:"
echo "sudo adduser vboxuser sudo"
# Update package lists and upgrade installed packages
echo "sudo apt update -y && sudo apt upgrade -y"
sudo apt update -y && sudo apt upgrade -y
# Install necessary packages
echo "sudo apt install xrdp -y"
sudo apt install xrdp -y
# Install optional packages, likely have already installed openssh-server and wget.  Excluded nano and curl here.
echo "sudo apt install openssh-server wget -y"
sudo apt install openssh-server wget -y

# Define the directory path
DIR="/tmp/scripts"

# Check if the directory exists
if [ ! -d "$DIR" ]; then
  echo "Directory $DIR does not exist. Creating it..."
  mkdir -p "$DIR"  # Create the directory, including any necessary parent directories
else
  echo "Directory $DIR already exists."
fi

# Change to the directory
cd "$DIR" || { echo "Failed to change directory to $DIR"; exit 1; }

# Configure firewall rules
rm -f configure_firewall_rules.sh
wget https://helloworld-wife-leftgithub-6685091628370c591e21e77a39229ff4c0a0.gitlab.io/assets/sh/configure_firewall_rules.sh
sudo dos2unix configure_firewall_rules.sh
sudo chmod a+x configure_firewall_rules.sh   #gives everyone execute permissions
sh configure_firewall_rules.sh

# Configure xrdp
#sudo systemctl status xrdp #This might be necessary unnless trouble-shooting required
sudo usermod -a -G ssl-cert xrdp 
sudo systemctl restart xrdp 
# Reboot and try rdp connection to static ipaddress
#sudo reboot
