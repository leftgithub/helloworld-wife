#!/bin/bash
# VirtualBox Setup Instructions (manual steps)
echo "VirtualBox Setup Instructions:"
echo "1) Install VirtualBox and Oracle VirtualBox Extension Pack."
echo "2) Add Bridged Adapter under Network Adapters."
echo "3) Set promiscuous mode to 'Deny' under Network Adapters, which is the default"
echo "4) Go to Settings -> Display -> Remote Display -> Enable Server."
echo "5) Go to Settings -> Display -> Remote Display -> Allow Multiple Connection -> Check the Checkbox"
echo "6) Go to Settings -> Display -> Screen -> Maximize the Video Memory."
echo "7) Go to Settings -> General -> Advanced -> Shared Clipboard -> Host to Guest"
echo "8) Manually remove IDE controller after setup to avoid CD/DVD-ROM errors when rebooting in Virtual Box"
echo "9) Manually removed floppy and optical from boot order under System in Virtual Box"
#might use dev-user, stg-user, and prd-user as example user names based on environment
#1) treat 192.168.0.0-192.168.79.255 as development
#2) treat 192.168.80.x through 192.168.99.x (192.168.80.0/20, 192.168.96.0/22) as staging
#3) treat 192.168.100.x through 192.168.255.x (192.168.100.0/22, 192.168.104.0/21, 192.168.112.0/20, 192.168.128.0/17) as production
#4) use subnet 192.168.0.0/16 in network configuration
#5) cannot use these ranges for #2 and #3 because my wifi subnet mask is 255.255.252.0
#6) after changing subnet mask to 255.255.0.0 for lan ip on wifi mesh router, I was able to use #2 and #3

#WORKING ON
#1) SSH keys for each environment -> create ssh key with git bash -> import public key to github -> convert private key using puttygen to use with putty


#preconditions
#0) install with neither Debian desktop environment nor GNOME nor standard utilities
#2) installed openssh_server during installion, but did not import ssh keys yet

#first priority is to install openssh-server, so that I can copy commands
#second priority is to install sudo and exit root user loging
#third priority is to install wget to be able to download scripts to run
#fourth priority is to install dos2unix to comvert windows line feeds to unix line feeds
su -
apt update
apt install -y openssh-server sudo wget dos2unix
#may need to grant sudo to user account to be able to call dos2unix

#1) manually ran commands from debian_install_sudo.sh or grant_sudo.sh
usermod -aG sudo vboxuser
#wget https://helloworld-wife-leftgithub-6685091628370c591e21e77a39229ff4c0a0.gitlab.io/assets/sh/debian/grant_sudo.sh

#cannot connect via ssh until I update the network adapters after taking a look at ip ad
sudo nano /etc/network/interfaces
#add the following lines: 
## The secondary network interface\n"
#auto enp0s8
#iface enp0s8 inet static
#address 192.168.56.210
#netmask 255.255.128.0
sudo systemctl restart networking #may fail with 255.255.0.0 even when router uses the netmask

#now can connect via ssh, configure ssh key via putty now that ssh is installed
mkdir -p ~/.ssh
chmod 700 ~/.ssh
wget https://github.com/impeachbrandon.keys
cat impeachbrandon.keys >> ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
sudo systemctl restart ssh
rm impeachbrandon.keys

#sudo timedatectl set-timezone America/New_York
sudo timedatectl set-timezone America/Chicago
#had major clock errors before installing ntp, which led to unnecessary cron job trouble-shooting
sudo apt install -y ntp
#sudo systemctl start ntp
#sudo systemctl enable ntp
sudo systemctl start ntpsec
sudo systemctl enable ntpsec
# add the following line to allow the system to step time if the offset is too large due to sleeping or shutdowns:
echo "tinker panic 0" | sudo tee -a /etc/ntpsec/ntp.conf


# Define the directory path
DIR="/tmp/scripts"

# Check if the directory exists
if [ ! -d "$DIR" ]; then
  mkdir -p "$DIR"  # Create the directory, including any necessary parent directories
fi

rm -f $DIR/download_scripts.sh
cd $DIR
wget https://helloworld-wife-leftgithub-6685091628370c591e21e77a39229ff4c0a0.gitlab.io/assets/sh/download_scripts.sh
sudo dos2unix $DIR/download_scripts.sh
sudo chmod a+x $DIR/download_scripts.sh
sh $DIR/download_scripts.sh


#detect the environment
ENVIRONMENT=$($DIR/detect_environment.sh)
echo $ENVIRONMENT

#12/1/2024 chose to set to production for now, need to update environment script based on domain
if [ "$ENVIRONMENT" = "Unknown" ]; then
  ENVIRONMENT="Production"
fi


# Check if the environment is "Unknown"
if [ "$ENVIRONMENT" = "Unknown" ]; then
  echo "Environment is Unknown. Halting script."
  exit 1
fi

sh $DIR/configure_firewall_rules.sh SSH #RDP

#either ran this during template creation or run it now
#1) manually ran configure_bridged_adapter_static_ipaddress.sh
#2) optionally manually tweaked the static ipaddress and sudo systemctl restart networking
#configure the network adapter firstssh -L 3389:localhost:3389 $USER@$(hostname)

#sh $DIR/configure_bridged_adapter_static_ipaddress.sh

#12/1/2024 decides not to configure desktop environment
#sh $DIR/configure_xrdp.sh $ENVIRONMENT
#sh $DIR/install_desktop_environment.sh $ENVIRONMENT

#block direct access to XRDP
sudo ufw deny 3389
sudo ufw reload

#to connect from windows
#open command prompt and create SSH tunnel
#for a single VM/tunnel
#ssh -L 3389:localhost:3389 vboxuser@your_debian_server
#ssh -L 3389:localhost:3389 vboxuser@192.168.56.250
#for multiple VMs and tunnels
#ssh -L 3389:localhost:3389 user@vm1 &
#ssh -L 3390:localhost:3389 user@vm2 &
#ssh -L 3391:localhost:3389 user@vm3 &
#ssh -L 3392:localhost:3389 user@vm4 &
#ssh -L 3393:localhost:3389 user@vm5 &
#ssh -L 3394:localhost:3389 user@vm6 &
#ssh -L 3395:localhost:3389 user@vm7 &
#ssh -L 3396:localhost:3389 user@vm8 &
#ssh -L 3397:localhost:3389 user@vm9 &
#ssh -L 3398:localhost:3389 user@vm10 &

#altenatively can use putty, but it annoyingly is prompting for my username and ssh passphrase

#(delay this until static ipaddress is permanently set), or have change environment code
#might want to add instructions for SSH tunnel
#parallel scripts execution setting banners
cat $DIR/filelist.txt | xargs -I {} -P 3 sh -c '{} $ENVIRONMENT'

#edit SSH configuration
sudo sed -i '1i PasswordAuthentication no' /etc/ssh/sshd_config
sudo sed -i '1i PermitEmptyPasswords no' /etc/ssh/sshd_config
sudo sed -i '1i PubkeyAuthentication yes' /etc/ssh/sshd_config
#sudo nano /etc/ssh/sshd_config
#disable password authentication
#    PasswordAuthentication no
#    PermitEmptyPasswords no
#Ensure SSH Key Authentication is Enabled
#    PubkeyAuthentication yes
#Restart the SSH Service
sudo systemctl restart ssh

#install ping
sudo apt install -y iputils-ping
#for nslookup, which may install more than is desired for security
#sudo apt install -y dnsutis

#need to override dhcp dns-nameserver settings, starting with two local dns servers
#because /etc/resolv.conf keeps getting overwritten by router
#default:
sudo sed -i '1i supersede domain-name-servers 10.0.1.133, 10.0.1.128, 208.67.222.222, 208.67.220.220, 8.8.8.8, 8.8.4.4;' /etc/dhcp/dhclient.conf 
#home lab:
#sudo sed -i '1i supersede domain-name-servers 192.168.56.250, 192.168.56.240, 208.67.222.222, 208.67.220.220, 8.8.8.8, 8.8.4.4;' /etc/dhcp/dhclient.conf

#install and configure apticron
#light-weight patch management tool that checks for package updates and sends email notifications, allowing administrators to manually schedule updates as needed.
#sh configure_postfix.sh #commented out on 12/1/2024: switched to nullmailer
sh configure_nullmailer.sh
sh configure_apticron.sh
sh configure_node_exporter.sh
sh generate_console_banner.sh $ENVIRONMENT
sh generate_ssh_banner.sh $ENVIRONMENT

chmod +x setup-sudo-apt.sh
sudo ./setup-sudo-apt.sh


sudo cp /tmp/scripts/update-upgrade.sh /usr/local/bin/update-upgrade.sh
sudo cp /tmp/scripts/configure_cron_jobs.sh /usr/local/bin/configure_cron_jobs.sh
sudo chmod +x /usr/local/bin/update-upgrade.sh
sudo chmod +x /usr/local/bin/configure_cron_jobs.sh
sudo sh /usr/local/bin/configure_cron_jobs.sh
sudo rm /usr/local/bin/configure_cron_jobs.sh



sh $DIR/remove_scripts.sh
sudo reboot