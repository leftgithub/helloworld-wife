#!/bin/


sudo ufw allow 53/tcp
sudo ufw allow 53/udp
sudo ufw reload

#Installing BIND
sudo apt update
#sudo apt install -y bind9 dnsutils #from article, which did not work
sudo apt install -y bind9 dnsutils bind9utils bind9-doc #from chatgpt

#The dnsutils package provides tools for testing and troubleshooting the DNS server.

#Configuring BIND
sudo nano /etc/bind/named.conf.options

#Defining Zones
sudo nano /etc/bind/named.conf.local

zone "yourdomain.com" {
    type master;
    file "/etc/bind/zones/db.yourdomain.com"; # zone file path
};

sudo mkdir /etc/bind/zones
sudo cp /etc/bind/db.local /etc/bind/zones/db.yourdomain.com
sudo nano /etc/bind/zones/db.yourdomain.com

#Edit the zone file with records like the following:

;
; BIND data file for yourdomain.com
;
$TTL    604800
@       IN      SOA     ns1.yourdomain.com. admin.yourdomain.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      ns1.yourdomain.com.
@       IN      NS      ns2.yourdomain.com
@       IN      A       192.168.0.1
ns1     IN      A       192.168.0.1
ns2     IN      A       192.168.0.2 @optional
www     IN      A       192.168.0.3 #optional
#Adjust the IP addresses and domain names as necessary for your setup.

#Starting BIND

#After configuration, start the BIND service:
#sudo systemctl start bind9

#Ensure that the service is enabled to start on boot:
#sudo systemctl enable bind9

#problem was that I had to use named.service and not bind9
sudo systemctl start named.service
sudo systemctl enable named.service

#need to override dhcp dns-nameserver settings, starting with two local dns servers
#because /etc/resolv.conf keeps getting overwritten by router
sudo sed -i '1i supersede domain-name-servers 192.168.56.250, 192.168.56.240, 208.67.222.222, 208.67.220.220, 8.8.8.8, 8.8.4.4;' /etc/dhcp/dhclient.conf

#configure the primary server zones and reverse zones
sudo nano /etc/bind/named.conf.local

zone "yourdomain.com" {
    type master;
    file "/etc/bind/db.yourdomain.com";
    allow-transfer { 192.168.56.240; };  # IP address of the secondary server
};

zone "56.168.192.in-addr.arpa" {
    type master;
    file "/etc/bind/db.192.168.56";
    allow-transfer { 192.168.56.240; };  # IP address of the secondary server
};

sudo systemctl restart bind9

#The master/slave replication is not working properly because of ipaddress changing, perhaps due to wifi router
#worked on manually editing other files such as /etc/bind/named.conf.options
#for a small network it is massively easier to manually edit two dns servers rather than trouble-shoot replication
#https://serverfault.com/questions/520666/bind-slave-doesnt-sync-up-with-master-until-it-is-restarted

#configure the secondary server
sudo nano /etc/bind/named.conf.local

zone "yourdomain.com" {
    type slave;
    file "/var/lib/bind/db.yourdomain.com";
    masters { 192.168.56.250; };  # IP address of the primary server
};

zone "56.168.192.in-addr.arpa" {
    type slave;
    file "/var/lib/bind/db.192.168.56";
    masters { 192.168.56.250; };  # IP address of the primary server
};

sudo systemctl restart bind9

#Finally, test your configuration for syntax errors:

sudo named-checkconf
sudo named-checkconf /etc/bind/named.conf
sudo named-checkconf /etc/bind/named.conf.options
sudo named-checkconf /etc/bind/named.conf.local
sudo named-checkzone yourdomain.com /etc/bind/db.yourdomain.com
