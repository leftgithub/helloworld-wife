#!/bin/bash

# Get the distributor ID from lsb_release
DISTRO=$(lsb_release -i | awk -F: '{print $2}' | xargs)

# Check if the distributor ID is Debian
if [ "$DISTRO" = "Debian" ]; then
    MY_VARIABLE="This is Debian, so will edit /etc/network/interfaces file"
else
    MY_VARIABLE="Error - only Debian is supported programmatically"
    exit 1
fi

# Output the value of MY_VARIABLE
echo "$MY_VARIABLE"


#to trouble-shoot ran this command in windows connecting to debian vm
#this downloads the /etc/network/interfaces file
#scp vboxuser@192.168.56.210:/etc/network/interfaces C:\Users\%username%\Downloads\

#altnerative to manually editing /etc/network/interfaces
#sudo nano /etc/network/interfaces

# Get the last interface, assuming it is the bridged adapter
INTERFACE=$(ip -o link show | awk '{print $2}' | cut -d':' -f1 | tail -n 1)

# Define static IP configuration
STATIC_IP_ADDRESS=192.168.56.210
NET_MASK=255.255.0.0
#GATEWAY=192.168.56.1 #DO NOT INCLUDE GATEWAY SINCE INTERNET ACCESS VIA NAT

#DO NOT INCLUDE DNS SERVERS SINCE INTERNET ACCESS VIA NAT
#create a list of dns servers, being very careful not to duplicate any ipaddresses
#open dns
#OPEN_DNS="208.67.222.222 208.67.220.220"
#cloudflare (performance + privacy)
#CLOUDFLARE="1.1.1.1"
#quad9
#QUAD9="9.9.9.9 149.112.112.112"
#google
#GOOGLE="8.8.8.8 8.8.4.4"
#comodo secure dns (not top tier performance)
#COMODO="8.26.56.26 8.20.247.20"
#May have issues with conflicting ipaddresses due to use of DHCP at the router or firewall level
#To circumvent this, create ipaddress reservations on hardware that may cause ipaddress conflicts
# Append configuration to /etc/network/interfaces
{
    printf "\n# The secondary network interface\n"
    printf "auto %s\n" "$INTERFACE"
    printf "iface %s inet static\n" "$INTERFACE"
    printf "    address %s\n" "$STATIC_IP_ADDRESS"
    printf "    netmask %s\n" "$NET_MASK"
#    printf "    gateway %s\n" "$GATEWAY"
#    printf "    dns-nameservers %s %s %s %s %s\n" "$OPEN_DNS" "$CLOUDFLARE" "$QUAD9" "$GOOGLE" "$COMODO"
} | sudo tee -a /etc/network/interfaces

# Restart networking service
sudo systemctl restart networking

