#!/bin/bash

# Check if username parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <Username>"
  exit 1
fi

Username=$1

# Check if the script is running as root; if not, use `su -` to switch to root
if [ "$(id -u)" -ne "0" ]; then
  echo "Switching to root user..."
  su -c "$0 $1" # Run this script as root
  exit
fi

# Check if sudo is installed
if ! command -v sudo &> /dev/null; then
  echo "Sudo not found. Installing sudo..."
  apt update
  apt install -y sudo
fi

# Add user to the sudo group
usermod -aG sudo "$Username"

# Inform the user of the changes and suggest actions
echo "User $Username has been added to the sudo group."
echo "Please reboot the system or log out and log back in for the changes to take effect."

# Exit the root user shell
exit