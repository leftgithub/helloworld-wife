#!/bin/bash

#switch to root user
su -
#install sudo
apt update
apt install sudo
#add your username (using vboxuser in the template) to sudoers list
usermod -aG sudo vboxuser
#reboot, or logout or exit of both root user and user account
exit
#may need logout of user account or reboot