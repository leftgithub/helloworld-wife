#!/bin/bash
#https://saturncloud.io/blog/how-to-run-google-colab-locally-a-step-by-step-guide/

# Download the Anaconda installer script
wget "https://repo.anaconda.com/archive/Anaconda3-2024.06-1-Linux-x86_64.sh"

# Make the installer script executable
chmod +x Anaconda3-2024.06-1-Linux-x86_64.sh

# Install Anaconda silently (using -b flag) to the specified location
# Adjust the path as necessary
sudo sh Anaconda3-2024.06-1-Linux-x86_64.sh -b -p $HOME/anaconda

# Initialize Anaconda for bash (required to use 'conda' command)
$HOME/anaconda/bin/conda init bash

# Source the bashrc file to make the 'conda' command available in this script
source ~/.bashrc

# Create a new conda environment named 'colab_env'
$HOME/anaconda/bin/conda create --name colab_env -y

# Note: 'conda activate' does not work directly in non-interactive scripts
# Instead, suggest running the following commands manually to activate the environment:
#   source ~/.bashrc
#   conda activate colab_env

# Alternatively, provide instructions to the user
echo "To activate the new environment, run:"
echo "source ~/.bashrc"
echo "conda activate colab_env"

# Activate the environment and install Jupyter Notebook and JupyterLab
source ~/.bashrc
conda activate colab_env

conda run -n colab_env conda install -y -c conda-forge jupyter jupyterlab

#make sure have current version of pip and setuptools
pip install --upgrade pip setuptools
pip install google-auth google-auth-oauthlib google-auth-httplib2
conda install -y -c conda-forge google-colab

# Run Jupyter Notebook within the 'colab_env' environment
conda run -n colab_env jupyter notebook