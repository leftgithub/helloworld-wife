#VERY IMPORTANT TO REVIEW THESE THREE FILES MANUALLY
#VERY IMPORTANT TO REMOVE THE VM NAME FROM THE MAILNAME OF YOUR SYSTEM
sudo apt install -y nullmailer mailutils
sudo sh -c "echo $(hostname --fqdn) > /etc/nullmailer/defaultdomain"

#sudo sh -c "echo 'target-email-address@your-domain.net' > /etc/nullmailer/adminaddr"
sudo sh -c "echo '$USER@$(hostname -d)' > /etc/nullmailer/adminaddr"

#sudo sh -c "echo 'smtp.gmail.com smtp --auth-login --port=587 --starttls --user=your-name@gmail.com --pass=\"your 2fa token e.g. 1234 abcd 5678 efgh\"' > /etc/nullmailer/remotes"

sudo sh -c "echo 'smtp.gmail.com smtp --auth-login --port=587 --starttls --user=$USER@$(hostname -d) --pass=\"your 2fa token e.g. 1234 abcd 5678 efgh\"' > /etc/nullmailer/remotes"

sudo chmod 600 /etc/nullmailer/remotes

#echo "error" | NULLMAILER_NAME="Testsystem check" mail -s "This is just a test with nullmailer" "target-email-address@your-domain.net"
echo "error" | NULLMAILER_NAME="Testsystem check" mail -s "This is just a test with nullmailer" "$USER@$(hostname -d)"