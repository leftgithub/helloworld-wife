sudo apt update
sudo apt upgrade -y
sudo apt install -y python3
sudo apt install -y python3-pip
sudo apt install -y python3-venv  # Install venv if it's not already installed

sudo apt install -y unixodbc unixodbc-dev #need unixodbc and unixodbc-dev for pyodbc to work properly

#install odbc driver for sql server
#https://learn.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver16&tabs=alpine18-install%2Calpine17-install%2Cdebian8-install%2Credhat7-13-install%2Crhel7-offline

sudo apt install -y curl

# Debian 12
curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --dearmor -o /usr/share/keyrings/microsoft-prod.gpg

#Download appropriate package for the OS version
#Choose only ONE of the following, corresponding to your OS version

#Debian 12
curl https://packages.microsoft.com/config/debian/12/prod.list | sudo tee /etc/apt/sources.list.d/mssql-release.list

sudo apt-get update
sudo ACCEPT_EULA=Y apt-get install -y msodbcsql18


#may need to install nginx and gunicorn, or something else
#create virtual environment for testproject
python3 -m venv test_env
source test_env/bin/activate
pip install django #only installed/accessible under test_env

django-admin startproject testproject
cd testproject #accessible both outside and inside test_env

#https://www.howtoforge.com/guide-to-install-django-web-framework-on-debian-12/
#generate secret key
#python3 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
#edit settings.py
#nano testproject/settings.py
#update ALLOWED_HOSTS and SECRET_KEY at a minimum
python3 manage.py collectstatic
python manage.py runserver
python manage.py migrate #to apply unapplied migrations
python manage.py runserver

#create admin
python3 manage.py createsuperuser

#to run at port 8080 instead of default port 8000 after activation with source test_env/bin/activate
sudo ufw allow 80
sudo ufw allow 8080
python3 ~/testproject/manage.py runserver 0.0.0.0:8080

#test pageL
#http://192.168.10.15:8080
#admin console:
#http://192.168.10.15:8080/admin

#In the next step, you will configure Django to run in the background by utilizing the Gunicorn WSGI server and Supervisor process manager.
#may need to go back and install other dependencies from the article:

#sudo apt install -y curl build-essential python3-dev python3-pip python3-venv nginx supervisor postgresql libpq5 libpq-dev
#want to install postgresql on another vm
sudo apt install -y curl build-essential python3-dev python3-pip python3-venv nginx supervisor libpq5 libpq-dev

#alternative article: https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu
#sudo apt install python3-venv python3-dev libpq-dev postgresql postgresql-contrib nginx curl
#this suggests that I can remove build-essential and libpq5

pip install gunicorn wheel #wheel required to avoid legacy installer for psycopg2
pip install psycopg2 #python driver for connecting to postgresql, which required installing libpq5 and/or libpq-dev
#rather than installing psycopg2, may install psycopg2-binary
#test gunicorn's ability to serve the project
gunicorn --bind 0.0.0.0:8000 testproject.wsgi:application


sudo bash -c 'cat <<EOL > /etc/supervisor/conf.d/testapp.conf
[program:testapp]
command=/bin/bash -c "source /home/vboxuser/test_env/bin/activate && gunicorn -t 3000 --workers 3 --bind unix:/home/vboxuser/testproject/testapp.sock --timeout 300 testproject.wsgi:application -w 2"
directory=/home/vboxuser/testproject
user=vboxuser
group=www-data
autostart=true
autorestart=true
stdout_logfile=/home/vboxuser/testproject/testapp.log
stderr_logfile=/home/vboxuser/testproject/error.log
EOL'

sudo systemctl restart supervisor
#curl --unix-socket /home/vboxuser/testproject/testapp.sock 127.0.0.1
curl --unix-socket /home/vboxuser/testproject/testapp.sock http://localhost/

sudo bash -c 'cat <<EOL > /etc/systemd/system/gunicorn.socket
[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target
EOL'

sudo mkdir /run/gunicorn
sudo chown vboxuser:vboxuser /run/gunicorn  # Change 'vboxuser' to the user running Gunicorn
sudo chmod 755 /run/gunicorn


sudo bash -c 'cat <<EOL > /etc/systemd/system/gunicorn.service
[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=vboxuser
Group=www-data
WorkingDirectory=/home/vboxuser/testproject
ExecStart=/home/vboxuser/test_env/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn.sock \
          --timeout 300 \
          testproject.wsgi:application
[Install]
WantedBy=multi-user.target          
EOL'

sudo systemctl start gunicorn.socket
sudo systemctl enable gunicorn.socket

cd ~/testproject
#openssl req -x509 -newkey rsa:2048 -keyout mykey.key -out mycert.crt -days 365 -nodes -subj "/C=US/ST=Florida/L=Saint Cloud/O=Sandhill #8282 LLC/OU=IT Department/CN=django.8282.co.uk"
sudo bash -c 'cat <<EOL > openssl.cnf
[ req ]
distinguished_name = req_distinguished_name
req_extensions = v3_req
prompt = no

[ req_distinguished_name ]
C = US
ST = Florida
L = Saint Cloud
O = Sandhill 8282 LLC
OU = Eat the Cat
CN = django.8282.co.uk

[ v3_req ]
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = django.8282.co.uk
DNS.2 = 8282.co.uk
IP.1 = 192.168.56.247
EOL'
openssl req -x509 -newkey rsa:2048 -keyout mykey.key -out mycert.crt -days 365 -nodes -config openssl.cnf -extensions v3_req

#changes to support ssl on nginx for public website
#sudo apt update & sudo apt install -y certbot python3-certbot-nginx
#below command fails
#sudo certbot --nginx -d 8282.co.uk -d www.8282.co.uk -d django.8282.co.uk

#very important to use \$ instead of $ below or sudo nginx -t will fail

sudo bash -c 'cat <<EOL > /etc/nginx/sites-available/django
server {
    listen 443 ssl;
    server_name your.internal.domain;  # Replace with your actual internal domain

    proxy_connect_timeout 300s;  # Time to wait for a connection to the upstream server
    proxy_send_timeout 300s;     # Time to wait for sending the request to the upstream server
    proxy_read_timeout 300s;     # Time to wait for a response from the upstream server
    send_timeout 300s;           # Time to wait for sending data to the client

    ssl_certificate /home/vboxuser/testproject/mycert.crt;
    ssl_certificate_key /home/vboxuser/testproject/mykey.key;

    # Handle favicon requests
    location = /favicon.ico {
        autoindex off; #disable directory listing
        access_log off;
        log_not_found off;
        alias /home/vboxuser/testproject/testapp/static/testapp/assets/favicon/favicon.ico;
    }

    # Main application location
    location / {
        autoindex off; #disable directory listing
        root /home/vboxuser/testproject/testapp/templates/testapp;
        try_files \$uri \$uri/ @django;
    }

    # Serve static files from /static URL
    location /static/ {
        autoindex off; #disable directory listing
        alias /home/vboxuser/testproject/testapp/static/testapp/;
    }

    # Serve assets
    location /assets/ {
        autoindex off; #disable directory listing
        alias /home/vboxuser/testproject/testapp/static/testapp/assets/;
    }

    # Pass requests to Django application
    location @django {
        proxy_set_header Host \$http_host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_pass http://unix:/home/vboxuser/testproject/testapp.sock;
    }
}

server {
    listen 80;
    server_name your.internal.domain;  # Replace with your actual internal domain

    proxy_connect_timeout 300s;  # Time to wait for a connection to the upstream server
    proxy_send_timeout 300s;     # Time to wait for sending the request to the upstream server
    proxy_read_timeout 300s;     # Time to wait for a response from the upstream server
    send_timeout 300s;           # Time to wait for sending data to the client

    # Redirect HTTP to HTTPS
    return 301 https://$host$request_uri;  # Redirect all HTTP requests to HTTPS
}
EOL'

sudo chmod o+rx /home/vboxuser #annoying nginx had 403 errors without this

sudo ln -s /etc/nginx/sites-available/django /etc/nginx/sites-enabled/
sudo unlink /etc/nginx/sites-enabled/default
sudo nginx -t

sudo ufw delete allow 80
sudo ufw delete allow 8080
sudo ufw allow 'Nginx Full'
sudo systemctl restart nginx