#!/bin/bash

# Download the Miniconda installer script
wget "https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"

# Make the installer script executable
chmod +x Miniconda3-latest-Linux-x86_64.sh

# Install Miniconda silently (using -b flag) to the specified location
# Adjust the path as necessary
sudo sh Miniconda3-latest-Linux-x86_64.sh -b -p $HOME/miniconda

# Initialize Miniconda for bash (required to use 'conda' command)
$HOME/miniconda/bin/conda init bash

# Source the bashrc file to make the 'conda' command available in this script
source ~/.bashrc

# Create a new conda environment named 'colab_env'
$HOME/miniconda/bin/conda create --name colab_env -y

# Note: 'conda activate' does not work directly in non-interactive scripts
# Instead, suggest running the following commands manually to activate the environment:
#   source ~/.bashrc
#   $HOME/miniconda/bin/conda activate colab_env

# Alternatively, provide instructions to the user
echo "To activate the new environment, run:"
echo "source ~/.bashrc"
echo "$HOME/miniconda/bin/conda activate colab_env"
